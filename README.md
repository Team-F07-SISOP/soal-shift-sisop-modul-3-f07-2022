## Anggota Kelompok

Mohammad Fadhil Rasyidin Parinduri // 5025201131

Farrel Emerson // 5025201082

Samuel // 5025201187

### Soal Shift
- [link](https://docs.google.com/document/d/1w3tYGgqIkHRFoqKJ9nKzwGLCbt30HM7S/edit?rtpof=true)


Kendala:
1. Secara pengetahuan masih kurang mumpuni
2. Sulit menelusuri pthread
3. Melakukan fork dalam thread memiliki banyak celah (contoh : melakukan pekerjaan 2x)

# Laporan Resmi
### Soal Nomor 1
- [soal1.c](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-3-f07-2022/-/blob/main/soal1/soal1.c)


#### Kode di Main
##### Source code Int Main
```c
int main() {
    //zip & create dir
    for(int i = 0; i < 3; ++i) {
        pthread_create(&thread[i], NULL, &unzip_or_create_dir, NULL);
    }

    for(int i = 0; i < 3; ++i) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    //handling .txt
    for(int i = 3; i < 5; ++i) {
        pthread_create(&thread[i], NULL, &decoding, NULL);
    }

    for(int i = 3; i < 5; ++i) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    //zip
    for(int i = 5; i < 6; ++i) {
        pthread_create(&thread[i], NULL, &zip, NULL);
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    // unzip or create file
    for(int i = 6; i < 8; ++i) {
        pthread_create(&thread[i], NULL, &unzip_or_create_file, NULL);
    }

    for(int i = 6; i < 8; ++i) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    // final combine
    for(int i = 8; i < 9; ++i) {
        pthread_create(&thread[i], NULL, &final_combine, NULL);
        pthread_join(thread[i], NULL);
    }
    
    pthread_exit(thread);
    exit(0);
    return 0;
}
```

##### Penjelasan
Di kode main, pembuatan thread dan join thread dilakukan dengan for. Untuk setiap soal, buat thread lalu join dan lakukan `sleep(1)`. Ini bertujuan untuk membuat thread di bawah nya tidak tabrakan dan membuat hasilnya menjadi tidak ada (misalnya dalam pembuatan file hasil zip dari soal sebelumnya).


#### Soal 1A
##### Source Code 1A
```c
void *unzip_or_create_dir(void *args) {
    char *argv1[] = {"unzip", "-o", "music.zip", "-d", "music", NULL};
    char *argv2[] = {"unzip", "-o", "quote.zip", "-d", "quote", NULL};
    char *argv3[] = {"mkdir", "-p", "hasil", NULL};


    pthread_t id = pthread_self();
    int status;

    if(pthread_equal(id, thread[0])) {
        printf("-----UNZIP MUSIC------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv1);
        }
    }

    if(pthread_equal(id, thread[1])) {
        printf("-----UNZIP QUOTE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv2);
        }
    }

    if(pthread_equal(id, thread[2])) {
        printf("-----CREATE FILE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/mkdir", argv3);
        }
    }
}
```

##### Cara Pengerjaan
1. Membuat argv untuk setiap tugas yang akan dijalankan oleh thread
2. Di setiap thread nya, cek apakah `pthread_self() == thread[i]`, dengan i range 0-8.
3. Untuk setiap thread nya, lakukan fork untuk melakukan `execv` unzip dan mkdir.


#### Soal 1B
#### Source Code 1B
```c
// Bagian decoding
char* base64Decoder(char encoded[], int len_str) {
    char* decoded_string;
 
    decoded_string = (char*)malloc(sizeof(char) * 100);
 
    int i, j, k = 0;
 
    int num = 0;
 
    int count_bits = 0;
 
    for (i = 0; i < len_str; i += 4) {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++)
        {
             
            // make space for 6 bits.
            if (encoded[i + j] != '=')
            {
                num = num << 6;
                count_bits += 6;
            }
 
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');
 
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);
 
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);
 
            else if (encoded[i + j] == '+')
                num = num | 62;
 
            else if (encoded[i + j] == '/')
                num = num | 63;
 
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }
 
        while (count_bits != 0) {
            count_bits -= 8;
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }
 
    decoded_string[k] = '\0';
 
    return decoded_string;
}
// End bagian decoding

void *decoding(void *args) {
    pthread_t id = pthread_self();

    if(pthread_equal(id, thread[3])) {
        printf("-----DECODING MUSIC------\n");
        music_file = fopen("hasil/music.txt", "w");

        DIR *d;
        struct dirent *dir;
        
        d = opendir("music");
        if (d) {
            while ((dir = readdir(d)) != NULL) {
                if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                    char *filename = malloc(256);
                    sprintf(filename, "music/%s", dir->d_name);

                    FILE *fp = fopen(filename, "r");

                    if(fp == NULL) {
                        printf("Error reading file\n");
                    }

                    const unsigned MAX_LENGTH = 256;
                    char buffer[MAX_LENGTH];

                    while(fgets(buffer, MAX_LENGTH, fp)) {
                        fscanf(fp, "%s\n", buffer);

                        int len = strlen(buffer);

                        fprintf(music_file, "%s\n", base64Decoder(buffer, len));
                    }
                    fclose(fp);
                }
            }

            closedir(d);
        }

        fclose(music_file);
    }

    if(pthread_equal(id, thread[4])) {
        printf("-----DECODING QUOTE------\n");
        quote_file = fopen("hasil/quote.txt", "w");

        DIR *d;
        struct dirent *dir;
        
        d = opendir("quote");
        if (d) {
            while ((dir = readdir(d)) != NULL) {
               if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                    char *filename = malloc(256);
                    sprintf(filename, "quote/%s", dir->d_name);

                    FILE *fp = fopen(filename, "r");

                    if(fp == NULL) {
                        printf("Error reading file\n");
                    }

                    const unsigned MAX_LENGTH = 256;
                    char buffer[MAX_LENGTH];

                    while(fgets(buffer, MAX_LENGTH, fp)) {
                        fscanf(fp, "%s\n", buffer);

                        int len = strlen(buffer);

                        fprintf(quote_file, "%s\n", base64Decoder(buffer, len));
                    }
                    fclose(fp);
                }
            }

            closedir(d);
        }

        fclose(quote_file);
    }

    return NULL;
}
```

##### Cara Pengerjaan
1. Membuat function base64Decoder yang akan mengembalikan string hasil decode. Untuk referensi kode nya dapat dilihat di [sini](https://www.geeksforgeeks.org/decode-encoded-base-64-string-ascii-string/)
2. Di dalam function decoding, untuk decode music dan quote nya kurang lebih sama, hanya berbeda letak file nya saja. Pertama membuat `DIR *d` dan `struct dirent *dir` seperti di modul 2. Lalu setiap file yang non ./.., buka file nya -> decode -> masukkan hasil ke file baru
3. Close file


#### Soal 1C
#### Source Code 1C
```c
void *zip(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"zip", "-P", password, "-r", "hasil.zip", "hasil", NULL};

    if(pthread_self() == thread[5]) {
        printf("-----ZIPPING HASIL.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/zip", argv);
        }
    }
}
```

##### Cara Pengerjaan
1. Membuat argv untuk melakukan zip
2. Lakukan fork pada thread nya
3. Lakukan `execv`


#### Soal 1D
#### Source Code 1D
```c
void *unzip_or_create_file(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"unzip", "-o", "-P", password, "hasil.zip", NULL};

    if(pthread_equal(id, thread[6])) {
        printf("-----RE_UNZIP hasil.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv);
        }
    }

    if(pthread_equal(id, thread[7])) {
        printf("-----CREATE no.txt------\n");

        FILE *fp = fopen("no.txt", "w");
        char *str = "No";

        fprintf(fp,"%s", str);

        fclose(fp);
    }
}
```

##### Cara Pengerjaan
1. Membuat argv untuk unzip
2. Untuk thread melakukan unzip, lakukan fork dan jalankan `execv`
3. Untuk thread bagian no.txt, lakukan open file no.txt, dan masukkan string "No" ke file dengan `fprintf`


#### Soal 1E
#### Source Code 1E
```c
void *final_combine(void *args) {
    if(pthread_equal(pthread_self(), thread[8])) {
        printf("-----FINAL COMBINE------\n");

        child = fork();

        if(child == 0) {
            char *argv[] = {"zip", "-P", password, "hasil.zip", "hasil", "no.txt", NULL};
            execv("/bin/zip", argv);
        }
    }
}
```

##### Cara Pengerjaan
1. Lakukan fork pada thread nya
2. Lakukan zip untuk hasil sebelumnya dan no.txt

### Soal Nomor 2
- [client.c](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-3-f07-2022/-/blob/main/soal2/Client/client.c)
- [server.c](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-3-f07-2022/-/blob/main/soal2/Server/server.c)

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

#### Soal 2A

Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:

isi users.txt:
username:password
username2:password2

##### Cara Pengerjaan dan Source Code 2A
```bash
/* Server Side */

void auth_handler(int new_socket){
    char choice[sz10b];
    valread = read(new_socket , choice, sz10b);
    if (choice[0] == 'r'){
        regis_handler(new_socket);
    }else if (choice[0] == 'l'){
        login_handler(new_socket);
    }
}
```

Dilakuakan pemilihan request dari client yang kemudian diterima server.

```bash
/* Client Side */

void auth(){
    char choice[1];
    printf("type r for register, l for login\n");
    scanf("%s", choice);
    printf("Dont type anything, wait until another client disconnect!\n");
    write(sock, choice, sz10b);
    if (choice[0] == 'r'){
        regist();
    }else if (choice[0] == 'l'){
        login();
    }
}
```

Dilakuakan pemilihan request client yang akan dikirim ke server.

```bash
/* Server side */

void regis_handler(int new_socket){
    char line[sz10b], data_user[sz10b][sz]={};
    int idx=0;
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
    }else {
        printf("Username List:\n");
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            const char s[2] = ":";
            u = strtok(line, s);
            printf("-%s\n", u);
            strcpy(data_user[idx], u);
            idx++;
        }
        fclose(fp);
    }
    char temp_buff[sz10b]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, sz10b);
    write(new_socket, data_user, sizeof(data_user));

    char user[sz10b]={},
         pass[sz10b]={};

    valread = read(new_socket, user, sz10b);
    valread = read(new_socket, pass, sz10b);
    fp = fopen(users_file,"a");
    fprintf(fp, "%s:%s\n", user, pass);
    printf("User [%s] Registered\n", user);
    fclose(fp);
}
```

Pada server side diatas diambil nama nama username dari file `users.txt` kemudian dikirimkan ke client side untuk dicek jika terdapat client baru yang register dengan username yang sama dengan database user. Kemudian mengambil username dan password dari client untuk disimpan ke database server.

```bash
/* Client Side */

void regist(){
    bool p_num = true, p_up = true, p_low = true, u_=false;
    char c, user[sz]={}, pass[sz]={}, temp_buff[sz10b]={};
    char data_user[sz10b][sz]={};
    int idx, i;
    valread = read(sock, temp_buff, sz10b);
    idx = string_to_int(temp_buff);
    valread = read(sock, data_user, sizeof(data_user));
    printf("Server free, you may type now\n");
    printf("Username: ");
    while(!u_){
        scanf("%s", user);
        for (i = 0; i < idx; i++){
            if (!strcmp(data_user[i], user)){
                u_=true;
                break;
            }
        }
        if (u_){
            u_ = false;
            printf("Username already exist\nUsername: ");
        }else {
            u_ = true;
            getchar();
        }
    }

    printf("Password: ");
    while(p_num || p_low || p_up) {
        c = getchar();
        while (c > 31){
            if (c == 32) {c = getchar(); continue;}
            if (p_num == true && c>47 && c<58) p_num = false;
            if (p_up == true && c>64 && c<91) p_up = false;
            if (p_low == true && c>96 && c<123) p_low = false;
            strncat(pass, &c, 1);
            c = getchar();
        }
        if(p_num || p_low || p_up || strlen(pass) < 6){
            printf("\nPassword Incorrect, password must contain the following:\n");
            if (strlen(pass) < 6) printf("- Minimum 6 characters\n");
            if (p_num) printf("- A  number\n");
            if (p_low) printf("- A  lowercase letter\n");
            if (p_up) printf("- An uppercase letter\n");
            p_num = p_up = p_low = true;
            memset(pass, '\0', sz);
            printf("Password: ");
        }
    }
    write(sock, user, sz10b);
    write(sock, pass, sz10b);
}
```

Pada client side akan mengambil data user dari database server untuk sebagai pengecekan nilai unique untuk username. Untuk password terdapat pengecekan sesuai permintaan soal jika sudah sesuai maka username dan password akan dikirim ke server.

#### Soal 2B

Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

##### Cara Pengerjaan dan Source Code 2B
```bash
/* Server Side */

void make_tsv(){
    FILE* fp;
    if (fp = fopen(database, "r")){
    }else{
        fp = fopen(database, "a");
        fprintf(fp, "Judul\tAuthor\n");
    }
    fclose(fp);
}

void *client_connect_handler(void *sock){
    ...
    make_tsv();
    ...
}
```

Pada `make_tsv()` dilakukan pengecekan file database terlebih dahulu, jika belum ada maka dibuat dan dilakukan pembuatan kolom. Jika sudah ada maka dibiarkan.

#### Soal 2C

Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
Contoh:

| Client-side |
| ----------- |
| add         |

| Server-side               |
| ------------------------- |
| Judul problem:            |
| Filepath description.txt: |
| Filepath input.txt:       |
| Filepath output.txt:      |

| Client-side              |
| ------------------------ |
| judul-problem-1          |
| Client/description.txt-1 |
| Client/input.txt-1       |
| Client/output.txt-1      |

Seluruh file akan disimpan oleh server ke dalam folder dengan nama `<judul-problem>` yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.

##### Cara Pengerjaan dan Source Code 2C
```bash
/* Server Side */

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    if (!strcmp(com, "add")){
        printf("User [%s] adding a problem\n", userlog);
        FILE* fp = fopen(database, "a");
        char question[4][sz] = {"Judul problem:", "Filepath description.txt:", "Filepath input.txt:", "Filepath output.txt:"};
        write2d(question, 4, new_socket);
        char serv_path[sz*2], client_path[sz*2], prob[sz]={};
        read(new_socket, client_path, sizeof(client_path));
        for(int i=0; i<4; i++){
            char buffer[sz10b]={}, src_path[sz10b]={}, dest_path[sz10b]={};
            valread = read(new_socket, buffer, sizeof(buffer));
            if (i == 0){
                struct stat st = {};
                while (stat(buffer, &st) != -1) {
                    write(new_socket, "error", sz);
                    valread = read(new_socket, buffer, sizeof(buffer));
                }
                if (stat(buffer, &st) == -1) write(new_socket, "success", sz);
                fprintf(fp, "%s\t%s\n", buffer, userlog);
                strcpy(prob, buffer);
                mkdir(buffer, 0700);
                chdir(buffer);
                if (getcwd(serv_path, 100*2) == NULL){
                    perror("error\n");
                }
            }else{
                strcat(dest_path, serv_path);
                strcat(dest_path, "/");
                strcat(dest_path, buffer);
                strcat(src_path, client_path);
                strcat(src_path, "/");
                strcat(src_path, buffer);
                download_file(src_path, dest_path);
            }
        }
        chdir("..");
        printf("Problem [%s] has been added by [%s]\n", prob, userlog);
        fclose(fp);
        return 1;
    }
    ...
}
```

Pada server akan mengambil command jika commandnya `add` maka pertanyaan akan dilemparkan ke client dan kemudian diambil jawabannya, setelah jawab diambil file file yang di upload client akan di masukkan ke dalam suatu problem dengan nama judul problem yang diinput client. Setelah itu nama dan author pembuat problem akan di ketik di file `problem.tsv`.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    if (!strcmp(com, "add")){
        char question[4][sz]={0};
        char client_dir[sz*2];
        getcwd(client_dir, sz*2);
        read2d(question, 4);
        write(sock, client_dir, sizeof(client_dir));
        for(int i=0; i<4; i++){
            char in[sz]={}, path[sz]={};
            printf("%s ", question[i]);
            scanf("%s", in);
            write(sock, in, sizeof(in));
            if (i==0){
                char temp[sz]={};
                read(sock, temp, sz);
                while(!(strcmp(temp, "error"))){
                    printf("Problem exist!\n%s ", question[i]);
                    scanf("%s", in);
                    write(sock, in, sizeof(in));
                    read(sock, temp, sz);
                }
            }
        }
        printf("\n");
        return 1;
    }
    ...
}
```

Pada client akan memasukan jawaban sesuai pertanyaan dari server kemudian jawab dikirimkan ke server dengan kriteria nama problem tidak boleh sama dengan yang sudah ada di server.

#### Soal 2D

Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

| judul-problem-1 by author1 |
| judul-problem-2 by author2 |

##### Cara Pengerjaan dan Source Code 2D
```bash
/* Server Side */

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "see")){
        printf("User [%s] see problem list\n", userlog);
        FILE* fp = fopen(database, "r");
        char author[sz10b][sz]={}, problem[sz10b][sz]={};
        fscanf(fp, "%s %s\n", problem[0], author[0]);
        int idx = 0;
        while(fscanf(fp, "%s\t%s", problem[idx], author[idx]) == 2){
            idx++;
        }
        writeInt(idx, sz, new_socket);
        for(int i=0; i<idx; i++){
            write(new_socket, problem[i], sz);
            write(new_socket, author[i], sz);
        }
        fclose(fp);
        return 1;
    }
    ...
}
```

Pada server akan membuka file `problem.tsv` dan membaca semua isinya kemudian datanya di lempar ke client.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "see")){
        char author[sz], problem[sz], buffer[sz];
        printf("Problem list:\n");
        valread = read(sock, buffer, sz);
        int idx = string_to_int(buffer);
        for(int i=0; i<idx; i++){
            read(sock, problem, sz);
            read(sock, author, sz);
            printf("-> %s by %s\n", problem, author);
        }
        printf("\n");
        return 1;
    }
}
```

Pada client semua data yang di ambil dari server akan di print sesuai format yang diminta.

#### Soal 2E

Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

##### Cara Pengerjaan dan Source Code 2E
```bash
/* Server Side */

void download_file(char *src_path, char *dst_path){
    int src_fd, dst_fd, n, err;
    unsigned char buffer[sz10b*10];

    src_fd = open(src_path, O_RDONLY);
    dst_fd = open(dst_path, O_CREAT | O_WRONLY);

    while (1) {
        err = read(src_fd, buffer, sz10b*10);
        if (err == -1) {
            printf("Error reading file.\n");
            exit(1);
        }
        n = err;

        if (n == 0) break;

        err = write(dst_fd, buffer, n);
        if (err == -1) {
            printf("Error writing to file.\n");
            exit(1);
        }
    }
    close(src_fd);
    close(dst_fd);
}

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "download")){
        printf("User [%s] download a problem\n", userlog);
        char problem[sz]={}, client_path[sz*2]={}, desc_path[sz*2]={}, in_path[sz*2]={}, serv_path[sz*2]={}, i_c_path[sz*2]={}, d_c_path[sz*2]={}, message[sz10b]={};
        FILE* fp;
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, client_path, sz*2);
        getcwd(serv_path, sz*2);
        strcat(desc_path, serv_path);
        strcat(desc_path, "/");
        strcat(desc_path, problem);
        strcat(desc_path, "/");
        strcat(in_path, desc_path);
        strcat(desc_path, "description.txt");
        strcat(in_path, "input.txt");
        if((fp = fopen(in_path, "r")) == NULL){
            strcpy(message, "Problem doesn't exist!");
            write(new_socket, message, sz10b);
            return 1;
        }
        strcat(client_path, "/");
        strcat(client_path, problem);
        mkdir(client_path, 0700);
        strcat(i_c_path, client_path);
        strcat(i_c_path, "/input.txt");
        strcat(d_c_path, client_path);
        strcat(d_c_path, "/description.txt");
        // printf("%s-\n", in_path);
        // printf("%s-\n", desc_path);
        // printf("%s-\n", i_c_path);
        // printf("%s-\n", d_c_path);
        if((fp = fopen(i_c_path, "r")) != NULL){
            fclose(fp);
            strcpy(message, "Problem has been already downloaded!");
            write(new_socket, message, sz10b);
            return 1;
        }
        download_file(in_path, i_c_path);
        download_file(desc_path, d_c_path);
        strcpy(message, "Problem successfully downloaded!");
        write(new_socket, message, sz10b);
        printf("Problem [%s] successfully downloaded by [%s]\n", problem, userlog);
        return 1;
    }
    ...
}
```

Pada server ketika client memberikan request `download`, server akan mengambil path client kemudian dilakukan exception jika problem yang diminta ada atau tidak dan juga jika tenyata client telah memiliki problem. Pada `download_file` dilakukan pembacaan sampai size yang dikirim ialah 0 atau pembacaan sudah tidak ada lagi.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "download")){
        char problem[sz]={}, path[sz*2]={}, message[sz10b]={};
        scanf("%s", problem);
        write(sock, problem, sz);
        getcwd(path, sz*2);
        write(sock, path, sz*2);
        valread = read(sock, message, sz10b);
        printf("%s\n\n", message);
        return 1;
    }
}
```

Pada client diambil pathnya kemudian dikirim ke server side. Setelah pemrosesan pada server akan dikirim feedback message berhasil / tidak nya mendownload problem.

#### Soal 2F

Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

##### Cara Pengerjaan dan Source Code 2F
```bash
/* Server Side */

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "submit")){
        printf("User [%s] submit a problem\n", userlog);
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2]={};
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, path, sz*2);
        getcwd(curr_path, sz*2);
        strcat(curr_path, "/");
        strcat(curr_path, problem);
        strcat(curr_path, "/");
        strcat(curr_path, "output.txt");
        FILE* fp = fopen(curr_path,"r");
        if(!fp) {
            printf("Unable to open %s\n", curr_path);
            exit(0);
        }
        char serv_out[sz10b][sz10b]={}, cli_out[sz10b][sz10b]={}, line[sz10b]={};
        int idx_serv=0, idx_cli=0;
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            u = strtok(line, "\n");
            strcpy(serv_out[idx_serv], u);
            idx_serv++;
        }fclose(fp);
        fp = fopen(path,"r");
        if(!fp) {
            printf("Unable to open %s\n", path);
            exit(0);
        }
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            u = strtok(line, "\n");
            strcpy(cli_out[idx_cli], u);
            idx_cli++;
        }fclose(fp);
        char message[sz]={};
        if(idx_serv != idx_cli) {
            strcpy(message, "WA");
            write(new_socket, message, sz);
            return 1;
        }
        for (int i=0; i<idx_serv; i++){
            printf("-%s-%s-\n", serv_out[i], cli_out[i]);
            if(strcmp(serv_out[i], cli_out[i])){
                strcpy(message, "WA");
                write(new_socket, message, sz);
                return 1;
            }
        }
        strcpy(message, "AC");
        write(new_socket, message, sz);
        return 1;
    }
    ...
}
```

Pada server side akan diambil isi dari `output.txt` client dan server. Kemudian pengecekan pertama pada index yang merupakan baris dari file jika banyak barisnya sudah berbeda antara `output.txt` server dan client maka akan dikirimkan message `WA` kepada client. Kemudian jika banyak baris sudah sama, selanjutnya akan dilakukan pengecekan isi string perbaris. Jika ada 1 yang berbeda maka akan dikirimkan message `WA` kepada client. Namun jika ternyata isinya sama maka akan dikirimkan message `AC`.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "submit")){
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2], message[sz];
        scanf("%s%s", problem, path);
        getcwd(curr_path, sz*2);
        write(sock, problem, sz);
        strcat(curr_path, "/");
        strcat(curr_path, path);
        write(sock, curr_path, sz*2);
        valread = read(sock, message, sz);
        printf("%s\n\n", message);
        return 1;
    }

}
```

Pada client side akan diambil path dari output yang disubmit kemudian dikirimkan ke server. Jika pemrosesan pada server telah selesai, akan dikirimkan feedback message dari server.

#### Soal 2G

Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

##### Cara Pengerjaan dan Source Code 2G
```bash
/* Server Side */

int main(){
    ...
    while((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))){
        printf("Connection accepted\n");

        pthread_t sniffer_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;

		if( pthread_create( &sniffer_thread , NULL ,  client_connect_handler , (void*) new_sock) < 0){
			perror("could not create thread");
			return 1;
		}

		pthread_join(sniffer_thread ,NULL);
		printf("\nDisconnected from client\n");
        printf("Waiting for client connections...\n");
    }
    ...
}
```

Untuk setiap koneksi akan diambil menggunakan `accept()` function. Untuk pemrosesan menggunakan thread dengan inisialisasi `pthread_create()` function, kemudian pada soal diminta untuk melakukan handle client satu per satu sehingga menggunakan `pthread_join()` function untuk menunggu hingga satu clinet / satu thread selesai.

#### Hasil Run

2A dan 2G
[![Screenshot-2022-04-17-16-06-45.png](https://i.postimg.cc/130Zs9SQ/Screenshot-2022-04-17-16-06-45.png)](https://postimg.cc/G4pfjn97)

2B
[![Screenshot-2022-04-17-16-11-44.png](https://i.postimg.cc/VNcLWGfY/Screenshot-2022-04-17-16-11-44.png)](https://postimg.cc/gxNWYKL5)

2C
[![image.png](https://i.postimg.cc/6QkWpMSs/image.png)](https://postimg.cc/v1vFPtbX)

2D
[![image-1.png](https://i.postimg.cc/WznTST4j/image-1.png)](https://postimg.cc/dZLbVMdS)

2E
[![image-2.png](https://i.postimg.cc/9FmW0Rxf/image-2.png)](https://postimg.cc/LhWd0XFd)

2F
[![image-3.png](https://i.postimg.cc/dt5JysXj/image-3.png)](https://postimg.cc/WhJRRvsd)

## Kendala

- Secara pengetahuan masih kurang mumpuni
- Sulit menelusuri pthread
- Melakukan fork dalam thread memiliki banyak celah (contoh: melakukan pekerjaan 2x)