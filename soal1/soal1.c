#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<pthread.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<dirent.h>

pthread_t thread[10];
pid_t child;
FILE *music_file;
FILE *quote_file;
char *password = "mihinomenestsamuel";

void *unzip_or_create_dir(void *args) {
    char *argv1[] = {"unzip", "-o", "music.zip", "-d", "music", NULL};
    char *argv2[] = {"unzip", "-o", "quote.zip", "-d", "quote", NULL};
    char *argv3[] = {"mkdir", "-p", "hasil", NULL};


    pthread_t id = pthread_self();
    int status;

    if(pthread_equal(id, thread[0])) {
        printf("-----UNZIP MUSIC------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv1);
        }
    }

    if(pthread_equal(id, thread[1])) {
        printf("-----UNZIP QUOTE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv2);
        }
    }

    if(pthread_equal(id, thread[2])) {
        printf("-----CREATE FILE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/mkdir", argv3);
        }
    }
}

// Bagian decoding
char* base64Decoder(char encoded[], int len_str) {
    char* decoded_string;
 
    decoded_string = (char*)malloc(sizeof(char) * 100);
 
    int i, j, k = 0;
 
    int num = 0;
 
    int count_bits = 0;
 
    for (i = 0; i < len_str; i += 4) {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++)
        {
             
            // make space for 6 bits.
            if (encoded[i + j] != '=')
            {
                num = num << 6;
                count_bits += 6;
            }
 
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');
 
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);
 
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);
 
            else if (encoded[i + j] == '+')
                num = num | 62;
 
            else if (encoded[i + j] == '/')
                num = num | 63;
 
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }
 
        while (count_bits != 0) {
            count_bits -= 8;
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }
 
    decoded_string[k] = '\0';
 
    return decoded_string;
}
// End bagian decoding

void *decoding(void *args) {
    pthread_t id = pthread_self();

    if(pthread_equal(id, thread[3])) {
        printf("-----DECODING MUSIC------\n");
        music_file = fopen("hasil/music.txt", "w");

        DIR *d;
        struct dirent *dir;
        
        d = opendir("music");
        if (d) {
            while ((dir = readdir(d)) != NULL) {
                if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                    char *filename = malloc(256);
                    sprintf(filename, "music/%s", dir->d_name);

                    FILE *fp = fopen(filename, "r");

                    if(fp == NULL) {
                        printf("Error reading file\n");
                    }

                    const unsigned MAX_LENGTH = 256;
                    char buffer[MAX_LENGTH];

                    while(fgets(buffer, MAX_LENGTH, fp)) {
                        fscanf(fp, "%s\n", buffer);

                        int len = strlen(buffer);

                        fprintf(music_file, "%s\n", base64Decoder(buffer, len));
                    }
                    fclose(fp);
                }
            }

            closedir(d);
        }

        fclose(music_file);
    }

    if(pthread_equal(id, thread[4])) {
        printf("-----DECODING QUOTE------\n");
        quote_file = fopen("hasil/quote.txt", "w");

        DIR *d;
        struct dirent *dir;
        
        d = opendir("quote");
        if (d) {
            while ((dir = readdir(d)) != NULL) {
               if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                    char *filename = malloc(256);
                    sprintf(filename, "quote/%s", dir->d_name);

                    FILE *fp = fopen(filename, "r");

                    if(fp == NULL) {
                        printf("Error reading file\n");
                    }

                    const unsigned MAX_LENGTH = 256;
                    char buffer[MAX_LENGTH];

                    while(fgets(buffer, MAX_LENGTH, fp)) {
                        fscanf(fp, "%s\n", buffer);

                        int len = strlen(buffer);

                        fprintf(quote_file, "%s\n", base64Decoder(buffer, len));
                    }
                    fclose(fp);
                }
            }

            closedir(d);
        }

        fclose(quote_file);
    }

    return NULL;
}

void *zip(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"zip", "-P", password, "-r", "hasil.zip", "hasil", NULL};

    if(pthread_self() == thread[5]) {
        printf("-----ZIPPING HASIL.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/zip", argv);
        }
    }
}

void *unzip_or_create_file(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"unzip", "-o", "-P", password, "hasil.zip", NULL};

    if(pthread_equal(id, thread[6])) {
        printf("-----RE_UNZIP hasil.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv);
        }
    }

    if(pthread_equal(id, thread[7])) {
        printf("-----CREATE no.txt------\n");

        FILE *fp = fopen("no.txt", "w");
        char *str = "No";

        fprintf(fp,"%s", str);

        fclose(fp);
    }
}

void *final_combine(void *args) {
    if(pthread_equal(pthread_self(), thread[8])) {
        printf("-----FINAL COMBINE------\n");

        child = fork();

        if(child == 0) {
            char *argv[] = {"zip", "-P", password, "hasil.zip", "hasil", "no.txt", NULL};
            execv("/bin/zip", argv);
        }
    }
}

int main() {
    //zip & create dir
    for(int i = 0; i < 3; ++i) {
        pthread_create(&thread[i], NULL, &unzip_or_create_dir, NULL);
    }

    for(int i = 0; i < 3; ++i) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    //handling .txt
    for(int i = 3; i < 5; ++i) {
        pthread_create(&thread[i], NULL, &decoding, NULL);
    }

    for(int i = 3; i < 5; ++i) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    //zip
    for(int i = 5; i < 6; ++i) {
        pthread_create(&thread[i], NULL, &zip, NULL);
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    // unzip or create file
    for(int i = 6; i < 8; ++i) {
        pthread_create(&thread[i], NULL, &unzip_or_create_file, NULL);
    }

    for(int i = 6; i < 8; ++i) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    // final combine
    for(int i = 8; i < 9; ++i) {
        pthread_create(&thread[i], NULL, &final_combine, NULL);
        pthread_join(thread[i], NULL);
    }
    
    pthread_exit(thread);
    exit(0);
    return 0;
}