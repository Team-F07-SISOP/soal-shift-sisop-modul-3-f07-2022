#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<pthread.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<dirent.h>

pthread_t thread[10];
pid_t child;

char *default_dir() {
    char *str = malloc(100);
    sprintf(str, "%s/shift3/", getenv("HOME"));

    return str;
}

char *working_dir() {
    char *str = malloc(100);
    sprintf(str, "%s/shift3/hartakarun", getenv("HOME"));

    return str;
}

void *createdir_and_zip(void *arg) {
    pthread_t id = pthread_self();
    char *argv[] = {"unzip", "hartakarun.zip", "-d", default_dir(), NULL};
    char *dd[] = {"mkdir", "-p", default_dir(), NULL};
    char *wd[] = {"mkdir", "-p", working_dir(), NULL};

    //create default dir
    if(pthread_equal(id, thread[0])) {
        child = fork();

        if(child == 0) {
            execv("/bin/mkdir", dd);
        }
    }

    //create working dir
    if(pthread_equal(id, thread[1])) {
        child = fork();

        if(child == 0) {
            execv("/bin/mkdir", wd);
        }
    }

    //unzip
    if(pthread_equal(id, thread[2])) {
        child = fork();

        if(child ==  0) {
            execv("/bin/unzip", argv);
        }
    }
}


int main() {
    for(int i = 0; i < 3; ++i) {
        pthread_create(&thread[i], NULL, &createdir_and_zip, NULL);
    }    

    for(int i = 0; i < 3; ++i) {
        pthread_join(thread[i], NULL);
    }

    printf("selesai sampai sini dulu :(\n");
}